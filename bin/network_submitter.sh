#!/bin/bash
network_script=$1
log=network_logs/${network_script%%.*}_log.txt
#check if a log exists, throw error
if [ -e $log ]
then
    echo "$log exists! not starting job. give it a new name first"
else
   sbatch slurm_submission.sh $network_script
   cp $network_script network_logs/
fi

