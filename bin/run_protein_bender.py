import argparse
import os
from protein_bender import Protein_Bender
from protein_bender import Network_Predictor, Random_Predictor


def get_arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config_file",
                        help="Path to the config file for this run",
                        default=None)
    parser.add_argument("-p", "--port",
                        help="Port to send prediction requests to",
                        default=None,
                        type=int)
    parser.add_argument("-f", "--protein_file",
                        help="Path to the protein file to bend",
                        default=None)
    parser.add_argument("-w", "--working_directory",
                        help="Path to the directory to save results",
                        default=os.getcwd())

    return parser.parse_args()


def main():

    arguments = get_arguments()

    protein_bender = Protein_Bender(arguments.protein_file)

    if arguments.config_file is not None:
        protein_bender.load_config_file(arguments.config_file)

    if arguments.working_directory is not None:
        protein_bender.working_directory = arguments.working_directory

    if arguments.port is not None:
        predictor = Network_Predictor(arguments.port)
    else:
        predictor = Random_Predictor(threshold=1.0)

    protein_bender.predictor = predictor

    protein_bender.start_bending()

    protein_bender.save()


if __name__ == "__main__":
    main()
