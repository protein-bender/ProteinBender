#!/bin/bash
#SBATCH --job-name=ProteinBender
#SBATCH --partition=gpu
#SBATCH --nodes=1
#SBATCH -t 24:00:00
#SBATCH --export=ALL
#SBATCH --gres=gpu:1
#SBATCH --mem=180G
#SBATCH -e ~/networks/network_logs/slurm_logs/%j_log.txt 
#SBATCH -o ~/networks/network_logs/slurm_logs/%j_log.txt

if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

HOME=/home/aaceves
source /home/aaceves/.bashrc

#module load $HOME/privatemodules/anaconda3
#module load $HOME/privatemodules/cuda/9.0

#export PATH=$HOME/bin:$PATH
#export PKG_CONFIG_PATH=/usr/share/pkgconfig:$PKG_CONFIG_PATH

echo current host: $HOSTNAME
python $1 &> ~/${1%%.*}"_"$SLURM_JOB_ID"_log.txt"
