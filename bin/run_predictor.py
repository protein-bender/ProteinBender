import argparse
import os
from ml.model import Model
from threading import Thread
from ml.model_manager import Model_Manager
from functools import partial
import signal


def get_arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument("--training_directory",
                        help="Path to existing training data",
                        default=None)
    parser.add_argument("--port",
                        help="Port to listen for training data on",
                        default=38338,
                        type=int)
    parser.add_argument("--model_directory",
                        help="Path to model working directory",
                        default=os.getcwd())

    return parser.parse_args()


def main():

    arguments = get_arguments()

    model = Model(arguments.model_directory)
    model_manager = Model_Manager(model, arguments.training_directory)

    signal.signal(signal.SIGINT, model_manager.stop)

    model_manager_thread = Thread(
        target=partial(model_manager.listen_for_clients, arguments.port))

    model_manager_thread.start()
    model_manager_thread.join()


if __name__ == "__main__":
    main()
