from distutils.core import setup

setup(
    name="protein_bender",
    version="0.1",
    packages=[
        "protein_bender",
        "protein_bender.protein_bender",
        "protein_bender.ml"
    ],
    install_requires=[
    ]
)
