# ProteinBender

![Bendy](https://imgur.com/pvK9E6I.png)

ProteinBender is a software package with a suite of tools for processing protein
structure-label mapping data and applying machine learning architectures to this
type of data for predicting labels. ProteinBender's initial primary use
case is for efficiently exploring protein energy landscapes to find lower energy
substitutions by using an online learning protocol that interfaces with
Protabit's Triad simulation software.

## Setup

The ProteinBender suite has two primary components: the biology modules, which
operate on protein data and the Triad software package, and the machine
learning modules, which operate with ML networks and agents. These have
distinct sets of requirements, but for using the full suite of tools, you will
need both.

Both sets of modules have been tested with: 
- Ubuntu 17.10
- GCC 7.2.0

### ProteinBender Bio Modules:

#### System Software

```
sudo apt-get install \
libpq-dev \
libblas-dev \
liblapack-dev \
gfortran \
gcc \
libboost-all-dev \
mesa-common-dev \
libglu1-mesa-dev \
freeglut3-dev \
libffi-dev \
libssl-dev
```

You will also need to install and compile Triad  
(https://triad.protabit.com/doc/user/index.html). Contact Protabit for the 
source code.

Triad comes with its own version of Python 2.7. To use ProteinBender Bio modules
you will need to setup your Python environment to work with Triad's packaged
Python.

### ProteinBender ML Modules:

ProteinBender's ML modules are written for Python 3, and rely on Keras for its
machine learning components. Thus, you will need Python 3.4+ and install
Keras

```
pip3 install numpy
pip3 install pandas
pip3 install keras
``` 

## Terminology

- Residue Type: The amino acid identity at a particular position
- Design Site: This is the site currently being operated on in a sequential
design experiment

## Main Components

The intended workflow of ProteinBender applications is generally as follows:

1. Generate data mapping protein structure to some label(s)
2. Transform that data into a voxelized format
3. Build an offline model to predict the label(s) from the voxelized data

From here, the designer can optionally continue in an online fashion:
1. Launch an online network model that listens for prediction requests and model
update information
2. Launch a protein designer agent that makes changes to a protein, and queries
the network model to make decisions on how to proceed in the protein design

TODO: Diagram here

Each of these elements are discussed in more detail below.

### Offline

#### Data generation

ProteinBender operates on protein structural information, coupled with some
label of interest. ProteinBender has utilities for taking a .pdb file and 
converting it into an [atom dictionary](#atom-dictionary). Users can augment
this data with their own label(s), or can use the included Triad wrappers to
generate labels from structures.

#### Data preprocessing

There are a variety of generic preprocessing tools available in ```utils/```,
and machine learning-specific preprocessing tools available
in ```ml/featurizer.py```.


Examples include:
- Extracting coordinate feature vectors from atom dictionaries
- Consistently one hot encoding atom and amino acid feature vectors
- Cropping and rotating coordinate feature vectors to clean up and augment 3D
  protein data
- Converting coordinate feature vectors into 4D voxel tensors

#### Model training

TODO

### Online

Online learning for ProteinBender involves two components: the machine learning
model, and the agent that interacts with the simulation environment. Since the
Triad interface software requires its own packaged version of Python 2.7, and
we want to be able to use the latest versions of Python and associated machine
learning packages (i.e. Keras), we separated the machine learning model and the
Triad agent. These processes can communicate online via socket connections,
and users can subclass the agent and model classes to create their own versions
of models/agents without having to implement their own socket communication.

#### Online model

To run models online, ProteinBender has a ```Model``` class in ```ml/model.py```
. Subclasses of ```Model``` should implement the following functions:

```add_training_example(self, features, label)```: This function will be called
whenever the class receives a training example over the socket. This will pass
in a 4D tensor voxel grid and associated training label.

```predict(self, features)```: This function will be called whenever the agent
makes a prediction request over the socket. This will pass in a 4D tensor voxel
grid and should return a predicted label value.

To use the model class, it must be constructed and then passed into a 
```Model_Manager``` to handle the socket connection. See example code for
details.

#### Online agent

ProteinBender's online agent is its namesake, and is the ```Protein_Bender```
class in ```protein_bender/protein_bender.py```.

To use ```Protein_Bender```, instantiate it with a path to a PDB file, and load
a config file with ```load_config_file```. For an example config file,
see ```examples/config/sample_config.json```

Once configured, ```Protein_Bender``` can be started with the
```start_bending``` function, which proceeds with the protein design simulation.
The native ```Protein_Bender``` class performs amino acid swaps at each of the
desired design sites, querying the model at each step to see if it is predicted
to be a productive amino acid swap, and proceeding with the time intensive relax
step only if that is the case.

Other protocols could be implemented by subclassing ```Protein_Bender```

## Data Formats

ProteinBender is designed to work by initially loading .pdb files from the
Protein Data Bank (https://www.rcsb.org/), but has intermediate data formats for 
more efficient access to the details of a PDB relevant for learning. The typical
workflow is:
1. Load file from PDB
2. Convert to atom-label dictionary for offline storage and online processing
3. Convert to voxelized format (with optional data augmentation) for learning

### PDB

PDB files are fixed-width text files, with one entry per line describing
different aspects of the protein structure.
 
The PDB file format specification can be found here: 
http://www.wwpdb.org/documentation/file-format.

ProteinBender operates at the atom and amino acid residue levels, so much of the
information in the PDB file is not necessary for downstream analysis.
Additionally, the fixed-width text format and information encoding is not
particularly friendly for parsing.

### Atom dictionary

For long-term storage and easy access to atomic and amino acid residue features,
ProteinBender has an atom dictionary format as below:

```
{
    "center": (x, y, z)
    "atoms": [
                {
                    "x_coord": x,
                    "y_coord": y,
                    "z_coord": z,
                    "designed_residue": Whether or not this is the design
                                        site
                    "residue_type": What residue this atom is a part of
                },
                ...
            ]
}
```

This can be obtained using the utility function ```convert_pdb_to_dict``` inside
```utils.triad```. This function relies internally on an initial conversion to a
Triad molecular system.

The "center" key refers to the user-defined center of the protein (usually the
design site), and is used for downstream rotations.

Typically, this object is pickled when saved to disk.

### Atom-label dictionary

In a typical ProteinBending use case, we want to associate the structure with
some value, such as energy or a functional metric, so that we can use the
protein data for machine learning or statistical analysis. To avoid the risk
of decoupling the structures from their associated values, we recommend using a
Python dictionary that contains the atom dictionary, augmented with any
associated labels. This format looks like this:

```
{
    "label_1": x_1,
    "label_2": x_2,
    "atoms": The atom dictionary
}
```

### Voxelized

For machine learning, we need to encode the information in a format conducive to
matrix operations. In order to capture 3D spatial information, we adopted a 4D 
tensor format, where the first three dimensions are the (x, y, z) coordinates
of the protein, and the last dimension is the features associated with that
voxel, such as the atomic identity or force field information. 

## Example Scripts

There are a variety of example scripts available in ```examples/```:

- ```pdb_io.py```: Loading, converting, and saving ProteinBender file formats
- ```coarse_swap_relax_analysis.py```: Performing a rotamer swap and seeing its 
effects on the relaxed energy, using sequential operations (local relax only)
- ```full_swap_relax_analysis.py```: Performing a rotamer swap and
seeing its effects on the relaxed energy, using intermediate saves to disk to
isolate the operations and perform a full backbone relax
- ```model_manager.py```: Launches a network model manager that listens for
prediction requests and new training data
- ```predictor_query.py```: Queries a network model manager and submits training
data to it
