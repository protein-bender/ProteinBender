from ml import featurizer
import struct

#import other peoples code
import numpy as np
from random import shuffle
import glob

#import keras stuff
import keras
from keras import backend as K
from keras.layers import Conv3D, MaxPooling3D, Flatten, Dropout, Input, Dense, ropout, Reshape, Permute, Activation, merge, ZeroPadding3D
from keras.models import Model, Sequential
from keras.callbacks import TensorBoard
from keras.utils.np_utils import to_categorical
from keras.constraints import maxnorm
from keras.optimizers import SGD, Adam

#read in a list of files in npy format, as they come out of the augmentator script
#we also experimented with augmenting on the fly, but found it to be a bottleneck and quickly discarded it
fileList = glob.glob('/central/scratchio/aaceves/augmented_data/*')
shuffle(fileList)

#adjust chunk size to maximize preformance
chunk_size = 7

def dataGenerator(files, batch_size):
    L = len(files)
    while True:

        batch_start = 0
        batch_end = batch_size

        while batch_start < L:
            limit = min(batch_end, L)
            subset = files[batch_start:limit]
            
            X = np.empty((0,20,20,20,47),int)
            Y = []
            for one_file in subset:
               temp = np.load(one_file)
               array = (temp.item().get('data'))
               array = array.reshape(1,20,20,20,47)
               X = np.append(X, array,axis=0)
               Y.append(temp.item().get('label'))

            yield (X,Y) 

            batch_start += batch_size   
            batch_end += batch_size


#networks are described in detail elsewhere.  Main point here is to show usage of generator
ES_callback = keras.callbacks.EarlyStopping(monitor='loss', min_delta=0.00001, patience=5, verbose=1, mode='auto')

input_all = Input(shape=(20,20,20,47), name='input')
x = Conv3D(128, (5, 5, 5), activation='relu', padding='same', name='block1_conv1', data_format="channels_last")(input_all)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block1_pool', data_format="channels_last")(x)

# Block 2
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block2_conv1', data_format="channels_last")(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block2_pool', data_format="channels_last")(x)

x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block3_conv1', data_format="channels_last")(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block3_pool', data_format="channels_last")(x)

x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block4_conv1', data_format="channels_last")(x)
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block4_conv2', data_format="channels_last")(x)
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block4_conv3', data_format="channels_last")(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block4_pool', data_format="channels_last")(x)

x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block5_conv1', data_format="channels_last")(x)
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block5_conv2', data_format="channels_last")(x)
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block5_conv3', data_format="channels_last")(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block5_pool', data_format="channels_last")(x)

x = Flatten(name='flatten')(x)
x = Dense(4096, activation='relu', name='fc1')(x)
x = Dropout(0.5)(x)
x = Dense(2048, activation='relu', name='fc2')(x)
#x = Dropout(0.5)(x)
main_output = Dense(1, activation='linear', name='predictions')(x)

model = Model(inputs=input_all, outputs=main_output)

adam = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0)

model.compile(optimizer=adam, loss='mean_squared_error')

#this is essential
model.fit_generator(dataGenerator(fileList,chunk_size), epochs=200, shuffle = True, callbacks=[ES_callback])

model_json = model.to_json()
with open("/central/scratchio/aaceves/model_output/model_big.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("/central/scratchio/aaceves/model_output/model_big.h5")
print("Saved model to disk")
