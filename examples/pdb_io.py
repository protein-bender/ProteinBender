import os
import numpy

from utils import utils as pb_utils
from utils import triad as pb_triad_utils

# The PDB file we want to read
pdb_file_path = "data/pdb/1pgaH.pdb"

# The structure-label dictionary we want to write out to
out_file_path = "data/pdb/1pgaH.pdb.dict"

# Which site we are designing around
site_to_design = "A_13"

# Read in a PDB and convert it into an atom dictionary
atom_dict = pb_triad_utils.convert_pdb_to_dict(pdb_file_path, site_to_design)

print("Center at: (%.2f, %.2f, %.2f)" % atom_dict["center"])
print("First atom at: (%.2f, %.2f, %.2f)" %
      (atom_dict["atoms"][0]["x_coord"], atom_dict["atoms"][0]["y_coord"],
       atom_dict["atoms"][0]["y_coord"]))


# Save as a structure-label dictionary - these can be labels that may be used
# for later learning
atom_dict["native_energy"] = numpy.random.random()
atom_dict["relaxed_energy"] = numpy.random.random()

# We use protocol 2 for reduced file size and compatibility with Python 2/3
pb_utils.save_to_file(atom_dict, out_file_path)

# Reload it - pickle figures out the protocol automatically
reloaded_dict = pb_utils.load_from_file(out_file_path)

# Just to make sure it worked
print("Center at: (%.2f, %.2f, %.2f)" % reloaded_dict["center"])
print("First atom at: (%.2f, %.2f, %.2f)" %
      (reloaded_dict["atoms"][0]["x_coord"],
       reloaded_dict["atoms"][0]["y_coord"],
       reloaded_dict["atoms"][0]["y_coord"]))

# Get rid of the file so this test script doesn't leave stuff around
os.unlink(out_file_path)
