
from wrappers import triad as pb_triad
from utils import triad as pb_triad_utils

site_to_design = "A_13"
pdb_file = "data/pdb/1pgaH.pdb"

before, after, molecular_system = pb_triad.get_energy_of_repack(
    site_to_design, pdb_file, num_trajectories=5)

print("Before: %.4f" % before)
print("After: %.4f" % after)
print("Delta: %.4f%%" % (100 * ((after - before) / abs(before))))

atom_dict = pb_triad_utils.convert_molecular_system_to_dict(
    molecular_system, site_to_design)
