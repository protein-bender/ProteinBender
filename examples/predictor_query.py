import argparse
import os
from protein_bender import Network_Predictor
import numpy


def get_arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port",
                        help="Port to send prediction requests to",
                        default=None,
                        type=int)

    return parser.parse_args()


def main():

    arguments = get_arguments()

    predictor = Network_Predictor(arguments.port)

    print("Sending prediction request")
    prediction = predictor.predict(numpy.random.random((20, 20, 20, 47)))

    print("Received prediction value: %.2f" % prediction)

    inform_value = 50.2
    print("Sending inform request of value %.2f" % inform_value)

    predictor.inform(numpy.random.random((20, 20, 20, 47)), inform_value)


if __name__ == "__main__":
    main()
