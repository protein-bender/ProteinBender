import numpy as np
import glob
import os

fileList = glob.glob('/home/aaceves/largeStorage/proteinBending/normalized_data_5_29/*.npy')

#here the script takes in the id's (PDB codes + chain indetifiers + residue numbers) that will compromise the validation set
#we get those by running the following bash one liner pointed at the folder with voxelized/augmented data: 
#ls augmented-voxelized-data/* | cut -d'_' -f4,7,8 | sort | uniq | sort -R | head -n ###REPLACE WITH DESIRED NUMBER### | sed ':a;N;$!ba;s/\n/\x27,\x27/g' | xclip -selection clipboard

val_list=['4j7e_A_27', '5epl_A_8', '4up5_A_336', '3kiv_A_0', '3uw5_A_86', '2vpe_A_342', '3gta_A_85', '2vpe_A_343', '3uw5_A_79', '4zyf_A_21', '3kiv_A_12', '4mn3_A_17', '5epl_A_21', '3g19_A_45', '4j7e_A_31', '4j47_A_159', '4j7d_A_26', '3uw4_A_260', '4up5_A_328', '4zyf_A_18', '3gta_A_83', '3g19_A_41', '4mn3_A_15', '4zyf_A_20', '3g19_A_37', '4zyf_A_19', '2i3i_A_79', '3gta_A_79', '4j7e_A_22', '2i3i_A_84', '4j46_A_155', '4j45_A_152', '4erf_A_25', '4up5_A_331', '4j45_A_154']


X = []
Y = []
X_val = []
Y_val = []
namez = []
namez_val = []

for file_subset in fileList:
	temp_file = np.load(file_subset)
	each_name = os.path.basename(file_subset)
	design_name = each_name.split('_')[1]+'_'+each_name.split('_')[4]+'_'+str(each_name.split('_')[5])
	array = (temp_file.item().get('data'))
	
	if design_name in val_list:
		X_val.append(array)
		Y_val.append((temp_file.item().get('energy_before'),temp_file.item().get('energy_after')))
		namez_val.append(each_name)
	else:
		X.append(array)
		Y.append((temp_file.item().get('energy_before'),temp_file.item().get('energy_after')))
		namez.append(each_name)

np.save('chunked_training_data_5_30.npy', X)
np.save('chunked_training_labels_5_30.npy', Y)
np.save('chunked_training_namez_5_30.npy', namez)

np.save('chunked_validation_data_5_30.npy', X_val)
np.save('chunked_validation_labels_5_30.npy', Y_val)
np.save('chunked_validation_namez_5_30.npy', namez_val)
