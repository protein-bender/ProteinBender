from triadPython import *
from wrappers import triad as pb_triad
from utils import triad as pb_triad_utils
import numpy as np
import time
import pickle
import glob
import re
import os
import sys
from random import shuffle

#make the pattern include atom, the letter, and numbers, match regex. extract after
pattern = re.compile("^ATOM.{17}[A-Z][ 0-9]{4}")

#need to get pdb file from the commandline, this allows for Triad parallel processing
pdb_file = sys.argv[-1]

sites_to_design = []
with open(pdb_file) as f:
    for line in f:
        formatted_site =line[21:26]
        formatted_site = ' '.join(formatted_site.split())
        formatted_site =(formatted_site.replace(' ','_'))
        if pattern.match(line) and formatted_site not in sites_to_design:
            sites_to_design.append(formatted_site)

#process the sites in a random order
shuffle(sites_to_design)

for site in sites_to_design:
   WT, before, after, molecular_system = pb_triad.get_energy_of_repack_pdb(site, pdb_file, num_trajectories=50)

   if any(v is None for v in [WT, before, after, molecular_system]):
      continue

   atom_dict = pb_triad_utils.convert_molecular_system_to_dict(molecular_system, site)

   millis = int(round(time.time() * 1000))
   handoff = {"WT_energy":WT,"energy_before":before,"energy_after":after,"atoms":atom_dict,"stamp":millis}

   pdb_id =  (os.path.splitext(os.path.basename(pdb_file))[0])
   designedResnums = [site]
   outname = "/home/aaceves/PB_david_final/" + str(millis) + "_" + str(pdb_id) +"_" + str(designedResnums[0]) + ".pickle"
   with open(outname,'wb') as f:
      pickle.dump(handoff,f)
