import argparse
import os
import numpy

from ml import Random_Model
from ml import Model_Manager


def get_arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port",
                        help="Port to receive requests from",
                        default=None,
                        type=int)

    return parser.parse_args()


def main():

    arguments = get_arguments()

    model = Random_Model(os.getcwd())

    model_manager = Model_Manager(model)

    model_manager.listen_for_clients(arguments.port)


if __name__ == "__main__":
    main()
