import numpy as np
import time
import sys
import pickle
import glob
import os

import keras
from keras.layers import Conv3D, MaxPooling3D, Flatten, Dropout
from keras.layers import LeakyReLU
from keras.layers import Input, Dense
from keras.models import Model, Sequential
from keras.callbacks import TensorBoard
from keras.utils.np_utils import to_categorical
from keras.constraints import maxnorm
from keras.layers import Dropout, Reshape, Permute, Activation, merge, ZeroPadding3D
from keras.optimizers import SGD, Adam
from keras import backend as K


# Get the name of this file to save the logs in a folder with the same name
netname = os.path.basename(sys.argv[0])

# Create tensorboard callback, store tensorboard logs under logs/tblogs/netname, one folder per network logs 
# To call tensorcord cd to /logs and do: tensorboard --logdir=./tblogs
# To pipe it to localport 9000 do
# ssh -N -f -L localhost:9000:login2:6006 edaveiga@login2.hpc.caltech.edu


#################### Define network ##########################

ES_callback = keras.callbacks.EarlyStopping(monitor='loss', min_delta=1, patience=10, verbose=1, mode='min')
tb = keras.callbacks.TensorBoard(log_dir='./logs', histogram_freq=0, batch_size=32, write_graph=True, write_grads=False, write_images=False, embeddings_freq=0, embeddings_layer_names=None, embeddings_metadata=None)
input_all = Input(shape=(20,20,20,47), name='input')


x = Conv3D(128, (5, 5, 5), padding='same', name='block1_conv1', data_format="channels_last")(input_all)
x = LeakyReLU(alpha=0.01)(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block1_pool', data_format="channels_last")(x)

x = Conv3D(256, (3, 3, 3), padding='same', name='block2_conv1', data_format="channels_last")(x)
x = LeakyReLU(alpha=0.01)(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block2_pool', data_format="channels_last")(x)


x = Conv3D(256, (3, 3, 3), padding='same', name='block3_conv1', data_format="channels_last")(x)
x = LeakyReLU(alpha=0.01)(x)
x = Conv3D(256, (3, 3, 3), padding='same', name='block3_conv2', data_format="channels_last")(x)
x = LeakyReLU(alpha=0.01)(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block3_pool', data_format="channels_last")(x)


x = Conv3D(256, (3, 3, 3), padding='same', name='block4_conv1', data_format="channels_last")(x)
x = LeakyReLU(alpha=0.01)(x)
x = Conv3D(256, (3, 3, 3), padding='same', name='block4_conv2', data_format="channels_last")(x)
x = LeakyReLU(alpha=0.01)(x)
x = Conv3D(256, (3, 3, 3), padding='same', name='block4_conv3', data_format="channels_last")(x)
x = LeakyReLU(alpha=0.01)(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block4_pool', data_format="channels_last")(x)


x = Conv3D(256, (3, 3, 3), padding='same', name='block5_conv1', data_format="channels_last")(x)
x = LeakyReLU(alpha=0.01)(x)
x = Conv3D(256, (3, 3, 3), padding='same', name='block5_conv2', data_format="channels_last")(x)
x = LeakyReLU(alpha=0.01)(x)
x = Conv3D(256, (3, 3, 3), padding='same', name='block5_conv3', data_format="channels_last")(x)
x = LeakyReLU(alpha=0.01)(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block5_pool', data_format="channels_last")(x)


x = Flatten(name='flatten')(x)
x = Dense(4096, name='fc1')(x)
x = LeakyReLU(alpha=0.01)(x)
x = Dropout(0.9)(x)
x = Dense(2048, name='fc2')(x)
x = LeakyReLU(alpha=0.01)(x)
x = Dropout(0.9)(x)
x = Dense(2048, name='fc3')(x)
x = LeakyReLU(alpha=0.01)(x)
x = Dropout(0.9)(x)

main_output = Dense(1, activation='linear', name='predictions')(x)

model = Model(inputs=input_all, outputs=main_output)

adam = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0)

model.compile(optimizer=adam, loss='mean_squared_error')


####################### LOAD DATA AND TRAIN ############### 

X = np.load('/central/scratchio/aaceves/chunked_train_data_5_28.npy')
Y = np.load('/central/scratchio/aaceves/chunked_train_labels_5_28.npy')
Y = (( Y[:,0] - Y[:,1] ) / Y[:,0])
subtract_off = Y.min()
Y = Y - subtract_off

X_val = np.load('/central/scratchio/aaceves/chunked_val_data_5_28.npy')
Y_val = np.load('/central/scratchio/aaceves/chunked_val_labels_5_28.npy')
Y_val = (( Y_val[:,0] - Y_val[:,1] ) / Y_val[:,0])
Y_val = Y_val - subtract_off


# This makes the early stop callback
ES_callback = keras.callbacks.EarlyStopping(monitor='loss', min_delta=1, patience=10, verbose=1, mode='min')

# Get the name of this file to save the logs in a folder with the same name
netname = os.path.basename(sys.argv[0])

# Create tensorboard callback, store tensorboard logs under logs/tblogs/netname, one folder per network logs 
# To call tensorcord cd to /logs and do: tensorboard --logdir=./tblogs
# To pipe it to localport 9000 do
# ssh -N -f -L localhost:9000:login2:6006 edaveiga@login2.hpc.caltech.edu
tensorboard_callback = keras.callbacks.TensorBoard(log_dir='/home/edaveiga/ProteinBender/networks/logs/tblogs/'+netname, histogram_freq=0, batch_size=32, write_graph=True, write_grads=False, write_images=False, embeddings_freq=0, embeddings_layer_names=None, embeddings_metadata=None)

history = model.fit(X,Y, epochs=200, batch_size = 32, shuffle = True, validation_data = (X_val,Y_val), callbacks=[ES_callback,tensorboard_callback])
