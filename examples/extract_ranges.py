import numpy as np
import glob
i=0
max_array = np.zeros(47)
min_array = np.zeros(47)

for files in glob.glob('unnormalized_data_5_31/*'):
   temp = np.load(files)
   array = (temp.item().get('data'))
   arrayS = np.split(array, 47, axis = 3)
   for n in range(33,47):
     min_array[n] = min(min_array[n], arrayS[n].min())
     max_array[n] = max(max_array[n], arrayS[n].max())

   if i % 1000 == 0:
      print(i)
   i=i+1
   print(i)

range_array = max_array-min_array

np.save('min_array.npy', min_array)
np.save('range_array.npy', range_array)

#note workflow is run augmentor with n = 1 augmentations, and no normalization, then extract normalization criteria with this script and run
