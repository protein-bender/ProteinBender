import numpy
import pickle

from triadPython import GlobalOptionSet, finalize


def convert_pdb_to_dict(pdb_file_path, center_site, file_out_path=None):
    """
    Reads in pdb_file_path and creates a dictionary of all atoms and
    the center of the structure. Dictionary is of the form:
    {
        "center": (x, y, z)
        "atoms": [
                    {
                        "x_coord": x,
                        "y_coord": y,
                        "z_coord": z,
                        "designed_residue": Whether or not this is the design
                                            site
                        "residue_type": What residue this atom is a part of
                    },
                    ...
                ]
    }

    If file_out_path is not None, writes the dictionary to a pickled file

    Returns the dictionary
    """

    options = GlobalOptionSet(ffset='crosetta')
    options.setAllRosettaOptions()
    options.optimization.setupAllInteractions = True

    fff, ms = finalize(options, pdb_file_path)

    atom_dict = convert_molecular_system_to_dict(ms, center_site)

    if file_out_path is not None:
        with open(file_out_path, 'wb') as out_file:
            pickle.dump(atom_dict, out_file, protocol=2)

    return atom_dict


def convert_molecular_system_to_dict(molecular_system, center_site):
    """
    Takes a molecular system and creates a dictionary of all atoms and
    the center of the structure. Dictionary is of the form:
    {
        "center": (x, y, z)
        "atoms": [
                    {
                        "x_coord": x,
                        "y_coord": y,
                        "z_coord": z,
                        "designed_residue": Whether or not this is the design
                                            site
                        "residue_type": What residue this atom is a part of
                    },
                    ...
                ]
    }

    Returns the dictionary
    """

    # this block is for extraction to voxelizer
    handoff_atoms = []
    center_x = []
    center_y = []
    center_z = []

    for atoms in molecular_system.atoms():

        residue = atoms.pid()
        residue_type = atoms.restype()

        if residue == str(center_site):
            mark_center = True
            center_x.append(atoms.coords.x)
            center_y.append(atoms.coords.y)
            center_z.append(atoms.coords.z)

        else:
            mark_center = False
        temp = (atoms.attributes)
        temp["x_coord"] = atoms.coords.x
        temp["y_coord"] = atoms.coords.y
        temp["z_coord"] = atoms.coords.z
        temp["designed_residue"] = mark_center
        temp["residue_type"] = residue_type
        handoff_atoms.append(temp)

    avg_center_x = numpy.mean(center_x)
    avg_center_y = numpy.mean(center_y)
    avg_center_z = numpy.mean(center_z)

    atom_dict = {"center": (avg_center_x, avg_center_y, avg_center_z),
               "atoms": handoff_atoms}

    return atom_dict