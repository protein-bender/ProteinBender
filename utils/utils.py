import numpy
import re
from ml import featurizer
import pickle

UNIQUE_ATOM_TYPES = ['C', 'H', 'MG', 'N', 'O', 'S', 'ZN']

# 26 residue types: the 20AA + HOH + Mg + Zn + the histitine protonation states HIE, HID, HSP
UNIQUE_RES_TYPES = ['ALA', 'ARG', 'ASN', 'ASP', 'CSS', 'CYH', 'GLN', 'GLU', 'GLY', 'HID', 'HIE', 'HOH', 'HSP',
                   'ILE', 'LEU', 'LYS', 'MET', 'MG', 'PHE', 'PRO', 'SER', 'THR', 'TRP', 'TYR', 'VAL', 'ZN']

PDB_SITE_REGEX_STRING = "^ATOM.{17}[A-Z][ 0-9]{4}"

# the lists fed to the one hot encoder will be sorted, so they should be in alphabetical order always
atom_type_key, atom_type_encoding = featurizer.convert_categorical_to_binary \
    (UNIQUE_ATOM_TYPES ,UNIQUE_ATOM_TYPES)
restype_key, restype_encoding = featurizer.convert_categorical_to_binary(
    UNIQUE_RES_TYPES, UNIQUE_RES_TYPES)


def get_available_sites_from_pdb(pdb_file_path):

    sites_to_design = []

    pattern = re.compile(PDB_SITE_REGEX_STRING)

    # This parses the PDB file and compiles a list of the sites
    with open(pdb_file_path) as pdb_file:
        for line in pdb_file:
            formatted_site = line[21:26]
            formatted_site = ' '.join(formatted_site.split())
            formatted_site = (formatted_site.replace(' ', '_'))
            if pattern.match(line) and formatted_site not in sites_to_design:
                sites_to_design.append(formatted_site)

    return sites_to_design


def convert_atom_dict_to_coordinate_feature_list(atom_dict):

    center = atom_dict.get('center')
    atoms = atom_dict.get('atoms')

    coord_array = []
    atom_array = []
    for each_atom in atoms:
        x = each_atom.get('x_coord')
        y = each_atom.get('y_coord')
        z = each_atom.get('z_coord')

        atom_type = each_atom.get('elem')
        # throw error if we encounter an atom different than ['C', 'H', 'O', 'N', 'S','MG', 'ZN']
        if atom_type not in UNIQUE_ATOM_TYPES:
            raise ValueError("Atom type %i is not expected!" % atom_type)

        atom_restype = each_atom.get('residue_type')
        if atom_restype not in UNIQUE_RES_TYPES:
            raise ValueError("Residue type %i is not expected!" % atom_restype)

        coord_array.append([x, y, z])

        if 'phoenix_charge' not in each_atom:
            each_atom['phoenix_charge'] = 0

        this_atom_ffparams = [
            each_atom['dreiding_BR'],
            each_atom['dreiding_EA'],
            each_atom['dreiding_LJ_D0'],
            each_atom['dreiding_LJ_R0'],
            each_atom['dreiding_LJ_scale'],
            each_atom['phoenix_charge'],
            each_atom['radius'],
            each_atom['rosetta_lj_d0'],
            each_atom['rosetta_lj_r0'],
            each_atom['rosetta_lj_r0_soft'],
            each_atom['rosetta_lk_dgfree'],
            each_atom['rosetta_lk_lambda'],
            each_atom['rosetta_lk_volume'],
            each_atom['scwrl_radius']
        ]

        # create the list of features for this atom
        this_atom_features = []
        # entries 0,1,2 are coordinates x,y,z
        this_atom_features.extend([x, y, z])
        # entries 3-10 are the 7 atom types one hot encoded, CHONS + Mg + Zn
        this_atom_features.extend(atom_type_encoding[atom_type_key[atom_type]])
        # entries 11-36 are the 26 residue types
        this_atom_features.extend(restype_encoding[restype_key[atom_restype]])
        # entires 37-50 are the 14 force field parameters to extracted above
        this_atom_features.extend(this_atom_ffparams)

        atom_array.append(this_atom_features)

    # checks for bad things, hopefully nothing happens
    bad_indicies = []
    idx = 0
    for types in atom_array:
        if types == None:
            bad_indicies.append(idx)
        idx = idx + 1
    for bad in bad_indicies:
        del atom_array[bad]
        del coord_array[bad]
    if bad_indicies:  # if the bad_indicies list is not empty, show bad atoms
        print('Found the following bad atoms:', bad_indicies)

    # remember entries 0,1,2 are X Y Z cartesian coordinates
    featurized_atoms = numpy.array(atom_array)
    # note that coord_array==featurized_atoms[:,0:3]
    coord_array = numpy.array(coord_array)
    center = numpy.array(center)

    return coord_array, featurized_atoms[:, 3:], center


def save_to_file(object_to_save, file_path):
    """
    A wrapper around pickle to ensure we use an efficient Python 2/3 protocol
    and close the file handle

    :param object_to_save: The object to write to disk
    :param file_path: Where to save it
    :return: Nothing
    """
    with open(file_path, "wb") as dump_file:
        pickle.dump(object_to_save, dump_file, protocol=2)


def load_from_file(file_path):
    """
    A wrapper around pickle to mirror save_to_file
    :param file_path: The file to load
    :return: The object
    """

    with open(file_path, "r") as load_file:
        object_to_load = pickle.load(load_file)

    return object_to_load
