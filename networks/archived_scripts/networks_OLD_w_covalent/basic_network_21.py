#CIFAR-10 basic variant
import numpy as np
import time
import sys
import pickle
import glob

import keras
from keras.layers import Conv3D, MaxPooling3D, Flatten, Dropout
from keras.layers import Input, Dense
from keras.models import Model, Sequential
from keras.callbacks import TensorBoard
from keras.utils.np_utils import to_categorical
from keras.constraints import maxnorm
from keras.layers import LeakyReLU

#
from keras.layers import Dropout, Reshape, Permute, Activation, merge, ZeroPadding3D
from keras.optimizers import SGD, Adam
from scipy.misc import imread, imresize, imsave
from keras import backend as K

from math import log10
X = np.load('/central/scratchio/aaceves/training_data_chunk.npy')
Y = np.load('/central/scratchio/aaceves/training_label_chunk.npy')

leaky_ReLU_activation = 0.2

ES_callback = keras.callbacks.EarlyStopping(monitor='loss', min_delta=1, patience=10, verbose=1, mode='min')


input_all = Input(shape=(20,20,20,47), name='input')
x = Conv3D(320, (3, 3, 3), strides=1, padding='same', name='layer_1', data_format="channels_last")(input_all)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Conv3D(320, (3, 3, 3), strides=1, padding='same', name='layer_2', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Conv3D(320, (3, 3, 3), strides=2, padding='same', name='layer_3', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Conv3D(640, (3, 3, 3), strides=1, padding='same', name='layer_4', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Dropout(0.1)(x)
x = Conv3D(640, (3, 3, 3), strides=1, padding='same', name='layer_5', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Dropout(0.1)(x)
x = Conv3D(640, (3, 3, 3), strides=2, padding='same', name='layer_6', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Conv3D(960, (3, 3, 3), strides=1, padding='same', name='layer_7', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Dropout(0.2)(x)
x = Conv3D(960, (3, 3, 3), strides=1, padding='same', name='layer_8', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Dropout(0.2)(x)
x = Conv3D(960, (3, 3, 3), strides=2, padding='same', name='layer_9', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Conv3D(1280, (3, 3, 3), strides=1, padding='same', name='layer_10', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Dropout(0.3)(x)
x = Conv3D(1280, (3, 3, 3), strides=1, padding='same', name='layer_11', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Dropout(0.3)(x)
x = Conv3D(1280, (3, 3, 3), strides=2, padding='same', name='layer_12', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Conv3D(1600, (3, 3, 3), strides=1, padding='same', name='layer_13', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Dropout(0.4)(x)
x = Conv3D(1600, (3, 3, 3), strides=1, padding='same', name='layer_14', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Dropout(0.4)(x)
x = Conv3D(1600, (3, 3, 3), strides=2, padding='same', name='layer_15', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Conv3D(1920, (3, 3, 3), strides=1, padding='same', name='layer_16', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Dropout(0.5)(x)
x = Conv3D(1920, (3, 3, 3), strides=1, padding='same', name='layer_17', data_format="channels_last")(x)
x = LeakyReLU(leaky_ReLU_activation)(x)
x = Dropout(0.5)(x)
x = Flatten(name='flatten')(x)
main_output = Dense(1, activation='tanh', name='predictions')(x)

model = Model(inputs=input_all, outputs=main_output)

adam = Adam(lr=0.00001)

model.compile(optimizer=adam, loss='mean_squared_error')

model.fit(X,Y, epochs=200, batch_size = 8, shuffle = True, callbacks=[ES_callback])
