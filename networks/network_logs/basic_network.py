import numpy as np
import time
import sys
import pickle
import glob

import keras
from keras.layers import Conv3D, MaxPooling3D, Flatten, Dropout
from keras.layers import Input, Dense
from keras.models import Model, Sequential
from keras.callbacks import TensorBoard
from keras.utils.np_utils import to_categorical
from keras.constraints import maxnorm

#
from keras.layers import Dropout, Reshape, Permute, Activation, merge, ZeroPadding3D
from keras.optimizers import SGD, Adam
from scipy.misc import imread, imresize, imsave
from keras import backend as K

from math import log10
X = np.load('/central/scratchio/aaceves/training_data_chunk_NoCovalent_list_method.npy')
Y = np.load('/central/scratchio/aaceves/training_label_chunk_NoCovalent_list_method.npy')
Y = Y - Y.min()


ES_callback = keras.callbacks.EarlyStopping(monitor='loss', min_delta=1, patience=10, verbose=1, mode='min')

input_all = Input(shape=(20,20,20,47), name='input')
x = Conv3D(128, (5, 5, 5), activation='relu', padding='same', name='block1_conv1', data_format="channels_last")(input_all)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block1_pool', data_format="channels_last")(x)

# Block 2
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block2_conv1', data_format="channels_last")(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block2_pool', data_format="channels_last")(x)

x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block3_conv1', data_format="channels_last")(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block3_pool', data_format="channels_last")(x)

x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block4_conv1', data_format="channels_last")(x)
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block4_conv2', data_format="channels_last")(x)
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block4_conv3', data_format="channels_last")(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block4_pool', data_format="channels_last")(x)

x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block5_conv1', data_format="channels_last")(x)
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block5_conv2', data_format="channels_last")(x)
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block5_conv3', data_format="channels_last")(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block5_pool', data_format="channels_last")(x)

x = Flatten(name='flatten')(x)
x = Dense(4096, activation='relu', name='fc1')(x)
x = Dropout(0.5)(x)
x = Dense(2048, activation='relu', name='fc2')(x)
#x = Dropout(0.5)(x)
main_output = Dense(1, activation='linear', name='predictions')(x)

model = Model(inputs=input_all, outputs=main_output)

adam = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0)

model.compile(optimizer=adam, loss='mean_squared_error')

model.fit(X,Y, epochs=200, batch_size = 32, shuffle = True, callbacks=[ES_callback])
