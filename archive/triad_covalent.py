"""

This script is meant to load a PDB file, pull all residue sites and loop over all of them.
In each loop, given a site it should:

1. Relax the entire structure with covalent terms ON.
2. Pick the best rotamer swap candidate for that site.
3. With the top rotamer in place, calculate the energy with the covalent terms OFF
4. Store this energy in a dictionary with key "before".
5. Store the XYZ coordinates of all atoms, centered around the site being designed with key "atoms"
6. Turn the covalent terms ON again and relax the region with the top rotamer
7. Turn covalent terms OFF, calculate the energy and store it with key "after"
8. Output the dictionary as a pickle file

We relax the structure at each step because we didn't know how to change the design site
in the molecular system object without reloading the PDB file.

"""

from triadPython import *
import numpy as np
import time
import pickle
import glob
import re
import os
import sys
from triadPython import StartFrom

# specify the input pdb file and loop definition... loop through all residues
#make the pattern include atom, the letter, and numbers, match regex, and extract after


# We can change it to any amino acid except cysteine
aas = ['ALA','ARG','ASN','ASP','GLU','GLN','GLY','HIS','ILE','LEU','LYS','MET',\
       'PHE','PRO','SER','THR','TRP','TYR','VAL']

site_to_design = "A_13"
sites_to_design = [site_to_design]
pdb_file = "data/pdb/1pgaH.pdb"
use_new_ms = True
if use_new_ms:
    print("Reloading new ms each optimization")

# This loops through all the sites extracted from the PDB file

########### INITIAL RELAXATION ################################

# Choose the rosetta force field with covalent terms for initial relax
opts = GlobalOptionSet(ffset='crosetta')
opts.allowRepulsiveReweighting()
opts.optimization.numTrajectories = 5
opts.rotamer.pedanticAttributes = True
opts.optimization.energyModel = EnergyModelType.Standard
opts.optimization.startFrom = StartFrom.Current
opts.optimization.saveLevel = SaveLevel.Accepted
opts.rotamer.singlesEnergyMax = None
opts.rotamer.allowCurrent = True

# Triad didn't do this in relaxOptions but we think we want to
opts.optimization.mover.preserveFormType = True

# Turn all terms on
opts.optimization.setupAllInteractions = True

# Make the sitemap
opts.optimization.siteMap = makeSiteMap(sites_to_design, aas)

# Create optimizer object
optimizer = MolecularOptimizer()

print("Finalizing")
fff, ms = finalize(opts, pdb_file)

print("Monomer is initially %s" % ms.getMonomer(site_to_design).getResType())


############ DO NOTHING FOR FIRST ENERGY THANG ########################
opts.optimization.selectMover(doNothing)

print("Optimizing")
optimization = optimizer.optimize(opts, fff=fff, ms=ms)

if use_new_ms:
    ms = optimization.getMolecularHierarchy()

print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())
print("Energy analysis after doNothing")
print("num_solutions: %i" % optimization.numSolutions())
optimization.energyAnalysis()

############ ENABLE COVALENT FOR WHATEVER ########################
opts.optimization.selectMover(enableCovalent)

print("Optimizing")

if use_new_ms:
    optimization = optimizer.optimize(opts, fff=fff, ms=ms)
    ms = optimization.getMolecularHierarchy()
else:
    optimization = optimizer.optimize(opts,fff=fff,ms=ms)

print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())
print("Energy analysis after enableCovalent")
print("num_solutions: %i" % optimization.numSolutions())
optimization.energyAnalysis()

############ DISABLE COVALENT IT BETTER FUCKING CHANGE SOMETHING ###############
opts.optimization.selectMover(disableCovalent)

print("Optimizing")

if use_new_ms:
    optimization = optimizer.optimize(opts, fff=fff, ms=ms)
    ms = optimization.getMolecularHierarchy()
else:
    optimization = optimizer.optimize(opts,fff=fff,ms=ms)

print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())
print("Energy analysis after disableCovalent")
print("num_solutions: %i" % optimization.numSolutions())
optimization.energyAnalysis()

############ ENABLE COVALENT FOR WHATEVER ########################
# opts.optimization.selectMover(enableCovalent)
#
# print("Optimizing")
#
# if use_new_ms:
#     optimization = optimizer.optimize(opts, fff=fff, ms=ms)
#     ms = optimization.getMolecularHierarchy()
# else:
#     optimization = optimizer.optimize(opts,fff=fff,ms=ms)
#
# print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())
# print("Energy analysis after enableCovalent")
# print("num_solutions: %i" % optimization.numSolutions())
# optimization.energyAnalysis()

############ RELAX ITLL BE OKAY ########################

opts.optimization.selectMover(multiRamp)

start = time.time()

# Relax the structure, covalent terms are on. This yields our relaxed PDB file.
if use_new_ms:
    optimization = optimizer.optimize(opts, fff=fff, ms=ms)
    ms = optimization.getMolecularHierarchy()
else:
    optimization = optimizer.optimize(opts,fff=fff,ms=ms)

end = time.time()

print("Relax duration: %.4f" % (end - start))

print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())
print("Energy analysis after relax")
print("num_solutions: %i" % optimization.numSolutions())
optimization.energyAnalysis()

############ DISABLE COVALENT IT BETTER FUCKING CHANGE SOMETHING ###############
opts.optimization.selectMover(disableCovalent)

print("Optimizing")

if use_new_ms:
    optimization = optimizer.optimize(opts, fff=fff, ms=ms)
    ms = optimization.getMolecularHierarchy()
else:
    optimization = optimizer.optimize(opts,fff=fff,ms=ms)

print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())
print("Energy analysis after disableCovalent")
print("num_solutions: %i" % optimization.numSolutions())
optimization.energyAnalysis()

sys.exit()
