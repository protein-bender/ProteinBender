#need to scale inputs, scale outputs, think about network structure in detail!!!
#then keras frameworks, triad alterations!

import numpy as np
from ml import featurizer
import time
import sys
import pickle
import struct
import glob

import keras
from keras.layers import Conv3D, MaxPooling3D, Flatten, Dropout
from keras.layers import Input, Dense
from keras.models import Model, Sequential
from keras.callbacks import TensorBoard
from keras.utils.np_utils import to_categorical
from keras.constraints import maxnorm

#
from keras.layers import Dropout, Reshape, Permute, Activation, merge, ZeroPadding3D
from keras.optimizers import SGD, Adam
from scipy.misc import imread, imresize, imsave
from keras import backend as K

from math import log10

fileList = glob.glob('/home/aaceves/largeStorage/proteinBending/ProteinBender-master/sampleTriadDataV2/*.pickle')

n_augmentations = 20

stepsPerEpoch = len(fileList) * n_augmentations

#We have CHONS + Mg + Zn in our proteins (and really nothing else)
unique_atom_types = ['C', 'H', 'MG', 'N', 'O', 'S', 'ZN']

# 26 residue types: the 20AA + HOH + Mg + Zn + the histitine protonation states HIE, HID, HSP
unique_restypes = ['ALA', 'ARG', 'ASN', 'ASP', 'CSS', 'CYH', 'GLN', 'GLU', 'GLY', 'HID', 'HIE', 'HOH', 'HSP',
            'ILE', 'LEU', 'LYS', 'MET', 'MG', 'PHE', 'PRO', 'SER', 'THR', 'TRP', 'TYR', 'VAL', 'ZN']

# the lists fed to the one hot encoder will be sorted, so they should be in alphabetical order always
atom_type_key, atom_type_encoding = featurizer.convert_categorical_to_binary(unique_atom_types ,unique_atom_types)
restype_key, restype_encoding = featurizer.convert_categorical_to_binary(unique_restypes ,unique_restypes)

def dataGenerator(files, batch_size):
####use this framework to augment on the fly!!!
    L = len(files)
    while True:

        batch_start = 0
        batch_end = batch_size

        while batch_start < L:
            limit = min(batch_end, L)
            subset = files[batch_start:limit]
            
            X = np.empty((0,20,20,20,47),int)
            Y = []
            for files in subset:
               temp = np.load(files)
               array = (temp.item().get('data'))
               array = array.reshape(1,20,20,20,47)
               X = np.append((X, array),axis=0)
               Y.append(temp.item().get('label'))

            yield (X,Y) #a tuple with two numpy arrays with batch_size samples     

            batch_start += batch_size   
            batch_end += batch_size

ES_callback = keras.callbacks.EarlyStopping(monitor='loss', min_delta=0.00001, patience=5, verbose=1, mode='auto')

input_all = Input(shape=(20,20,20,47), name='input')
x = Conv3D(128, (5, 5, 5), activation='relu', padding='same', name='block1_conv1', data_format="channels_last")(input_all)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block1_pool', data_format="channels_last")(x)

# Block 2
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block2_conv1', data_format="channels_last")(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block2_pool', data_format="channels_last")(x)

x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block3_conv1', data_format="channels_last")(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block3_pool', data_format="channels_last")(x)

x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block4_conv1', data_format="channels_last")(x)
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block4_conv2', data_format="channels_last")(x)
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block4_conv3', data_format="channels_last")(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block4_pool', data_format="channels_last")(x)

x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block5_conv1', data_format="channels_last")(x)
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block5_conv2', data_format="channels_last")(x)
x = Conv3D(256, (3, 3, 3), activation='relu', padding='same', name='block5_conv3', data_format="channels_last")(x)
x = MaxPooling3D(pool_size=(4, 4, 4), strides=(2, 2, 2), padding='same', name='block5_pool', data_format="channels_last")(x)

x = Flatten(name='flatten')(x)
x = Dense(4096, activation='relu', name='fc1')(x)
x = Dropout(0.5)(x)
x = Dense(2048, activation='relu', name='fc2')(x)
#x = Dropout(0.5)(x)
main_output = Dense(1, activation='linear', name='predictions')(x)

model = Model(inputs=input_all, outputs=main_output)

adam = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0)

model.compile(optimizer=adam, loss='mean_squared_error')

model.fit_generator(dataGenerator(fileList,16), epochs=200, steps_per_epoch = stepsPerEpoch, shuffle = True, callbacks=[ES_callback])

model_json = model.to_json()
with open("/output/model_big.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("/output/model_big.h5")
print("Saved model to disk")
