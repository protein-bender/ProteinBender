
from triadPython import *
import time
import sys
from triadPython import StartFrom
import numpy

# specify the input pdb file and loop definition... loop through all residues
#make the pattern include atom, the letter, and numbers, match regex, and extract after


# We can change it to any amino acid except cysteine
aas = ['ALA','ARG','ASN','ASP','GLU','GLN','GLY','HIS','ILE','LEU','LYS','MET',\
       'PHE','PRO','SER','THR','TRP','TYR','VAL']

site_to_design = "A_10"
sites_to_design = [site_to_design]
pdb_file = "data/pdb/1pgaH.pdb"

########### INITIAL RELAXATION ################################

# Choose the rosetta force field with covalent terms for initial relax
opts = GlobalOptionSet(ffset='crosetta')
opts.setAllRosettaOptions()
opts.job.randomSeed = numpy.random.randint(100000)
opts.allowRepulsiveReweighting()
opts.optimization.numTrajectories = 1
opts.rotamer.pedanticAttributes = True
opts.optimization.energyModel = EnergyModelType.Standard
opts.optimization.startFrom = StartFrom.Current
opts.optimization.saveLevel = SaveLevel.Accepted
opts.rotamer.singlesEnergyMax = None
opts.rotamer.allowCurrent = True

# Triad didn't do this in relaxOptions but we think we want to
opts.optimization.mover.preserveFormType = True

# Turn all terms on
opts.optimization.setupAllInteractions = True

# Make the sitemap
opts.optimization.siteMap = makeSiteMap(sites_to_design, aas)

# Create optimizer object
optimizer = MolecularOptimizer()

print("Finalizing")
fff, ms = finalize(opts, pdb_file)

print("Monomer is initially %s" % ms.getMonomer(site_to_design).getResType())

############ RELAX ITLL BE OKAY ########################

opts.optimization.selectMover(multiRamp)

start = time.time()

# Relax the structure, covalent terms are on. This yields our relaxed PDB file.
optimization = optimizer.optimize(opts, fff=fff, ms=ms)
ms = optimization.getMolecularHierarchy()

end = time.time()

print("Relax duration: %.4f" % (end - start))

print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())
print("Energy analysis after relax")
print("num_solutions: %i" % optimization.numSolutions())
optimization.energyAnalysis()

############ PACK THAT SHIT ########################

opts.optimization.startFrom = StartFrom.Random
opts.optimization.mover.preserveFormType = False

opts.optimization.selectMover(rosettaRepackingMover)

start = time.time()

# Now perform the top rotamer selection
optimization = optimizer.optimize(opts, fff=fff, ms=ms)
ms = optimization.getMolecularHierarchy()

end = time.time()

print("Repack duration: %.4f" % (end - start))

print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())
print("Energy analysis after rosettaRepackingMover")
print("num_solutions: %i" % optimization.numSolutions())
optimization.energyAnalysis()

############ RELAX IT'LL BE OKAY ########################
opts.optimization.selectMover(multiRamp)

# Relax the structure, covalent terms are on. This yields our relaxed PDB file.
optimization = optimizer.optimize(opts, fff=fff, ms=ms)
ms = optimization.getMolecularHierarchy()

print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())
print("Energy analysis after relax")
print("num_solutions: %i" % optimization.numSolutions())

optimization.energyAnalysis()

sys.exit()
