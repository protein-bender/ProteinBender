from ml import featurizer
import numpy
import time
import pickle

import numpy.distutils.system_info as sysinfo
print(sysinfo.get_info('blas'))
import numpy as np
np.__config__.show()

num_dimensions = 3
num_atoms = 100
num_augmentations = 20000
num_epochs = 1000
method = 'disk'

atom_types = ["H", "C", "N", "O"]

numpy.random.seed(42)
test_coordinates = numpy.random.normal(0, 1, (num_atoms, num_dimensions)).round(4)
atoms = numpy.random.choice(atom_types, num_atoms)

_, atoms = featurizer.convert_categorical_to_binary(atoms, atom_types)

start_time = time.time()

if method == 'disk':

    augmented_test_coordinates, center_array = featurizer.augment_dataset(test_coordinates, num_augmentations, numpy.array([10, 10, 10]))

    for i in range(augmented_test_coordinates):
        augmented_test_coordinates[i]


for coordinates in augmented_test_coordinates:
    voxel_grid = featurizer.voxelize(coordinates, atoms)

duration = time.time() - start_time

print("Duration: %.2f" % duration)

