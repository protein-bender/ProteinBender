from triadPython import *
import sys
import triad
from triad import opt
from opt import Finalize
from triad import util
from util import GlobalOptionSet


opts = GlobalOptionSet()
opts.forcefield.applySet("rosetta")
opts.setAllRosettaOptions()

designedResnums = [3,5,7,20]
aas = ['WT','ALA','VAL']
opts.optimization.siteMap = makeSiteMap(designedResnums,aas)


fff,ms = finalize(opts,["../data/pdb/1pga_H.pdb"])

optimizer = MolecularOptimizer()
solutions = optimizer.optimize(opts,fff,ms)

solutions.printConf()
solutions.getAnalysis(detailLevel=1)
print ('quak1')

solutions.writePDB(solutionNumber=3)
print ('quak2')

solutions.writeBinaryFile("mySolutions.solns")
print('quack3')
ea = EnergyAnalysis(opts,fff,ms)
print ea.getAnalysis()


print('quack4')
solutions.writeAllPDB()
print('quack5')
solutions.getAnalysis(numPDBToPrint=1,detailLevel=1)
print('quack6')
solutions.energyAnalysis(solutionNumber=0,
                         pids=["len(m.designed_restypes) > 0","total"])
print('quack7')


print solutions
print('quack8')