from triadPython import *
import numpy as np
import time
import pickle
import glob
import re
import os
import sys
# specify the input pdb file and loop definition... loop through all residues n times?
#do we want this to ultimately be run from within triad, or as a seperate program?

#make the pattern include atom, the letter, and numbers, match regex, and extract after
#pattern = re.compile("^A[ 0-9]{4}$")
pattern = re.compile("^ATOM.{17}[A-Z][ 0-9]{4}")
#need to get pdb file from the commandline!
#read it OUTSIDE of triad, because we operate on it here first
pdb_file = sys.argv[-1]

sites_to_design = []
with open(pdb_file) as f:
    for line in f:
        formatted_site =line[21:26]
        formatted_site = ' '.join(formatted_site.split())
        formatted_site =(formatted_site.replace(' ','_'))
        if pattern.match(line) and formatted_site not in sites_to_design:
            sites_to_design.append(formatted_site)

print(sites_to_design)

for site in sites_to_design:
    print(site)
    opts = GlobalOptionSet(ffset='crosetta')
    opts.setAllRosettaOptions()
    #working here?
    opts.optimization.setupAllInteractions = True

    #MUST SPECIFY IN CHAIN_NUMBER FORMAT... IT BECOMES IMPORTANT LATER ON
    #should we loop through all residues in a particular protein? we can train on scPDB
    designedResnums = [site]
    aas = ['ALA','ARG','ASN','ASP','GLU','GLN','GLY','HIS','ILE','LEU','LYS','MET','PHE','PRO','SER','THR','TRP','TYR','VAL']
    print(designedResnums)
    opts.optimization.siteMap = makeSiteMap(designedResnums,aas)

    #THIS WORKED, BUT INITIAL MOVE WAS WITH COVALENT ON. SHOULD FIRST "MOVE" BE TO DISABLE? DOES APPLY.MOVER RUN THE MOVER, OR JUST SET IT?
    #WHAT IS .getMolecularHeirarchy doing?
    opts.optimization.selectMover(disableCovalent)

    # do the loop optimization based on the wild-type sequence
    fff,ms = finalize(opts,pdb_file)
    optimizer = MolecularOptimizer()
    optimization = optimizer.optimize(opts,fff,ms)
    optimization.getAnalysis()

    opts.optimization.selectMover(rosettaRepackingMover)
    optimization = optimizer.optimize(opts,fff=None,ms=None,poseGenerators=[])

    optimization.getAnalysis()
    before = optimization.energyAnalysis(0)


    #this block is for extraction to voxelizer
    handoff_atoms=[]
    center_x = []
    center_y = []
    center_z = []
    designed_res_coords = []
    for atoms in ms.atoms():
        residue = str(atoms).split('/')[2].split('|')[1]
        residue_type = str(atoms).split('/')[2].split('|')[0]

        if residue == designedResnums[0]:
            mark_center = True
            center_x.append(atoms.coords.x)
            center_y.append(atoms.coords.y)
            center_z.append(atoms.coords.z)

        else:
            mark_center = False
        temp = (atoms.attributes)
        temp['x_coord'] = atoms.coords.x
        temp['y_coord'] = atoms.coords.y
        temp['z_coord'] = atoms.coords.z
        temp['designed_residue'] = mark_center
        temp['residue_type'] = residue_type
        handoff_atoms.append(temp)

    #this block is for extraction to voxelizer

    optimizer.applyMover(opts,enableCovalent)
    opts.optimization.selectMover(multiRamp)
    optimization = optimizer.optimize(opts,fff=None,ms=None,poseGenerators=[])
    optimization.getAnalysis()
    after = optimization.energyAnalysis(0)

    energy_change = before - after
    avg_center_x = np.mean(center_x)
    avg_center_y = np.mean(center_y)
    avg_center_z = np.mean(center_z)
    millis = int(round(time.time() * 1000))


    handoff = {"center":(avg_center_x,avg_center_y,avg_center_z),"energy":energy_change,"atoms":handoff_atoms,"stamp":millis}

    pdb_id =  (os.path.splitext(os.path.basename(pdb_file))[0])
    outname = "datafolder/" + str(millis) + "_" + str(pdb_id) +"_" + str(designedResnums[0]) + ".pickle"
    with open(outname,'wb') as f:
        pickle.dump(handoff,f)
