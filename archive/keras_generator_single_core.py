fileList = glob.glob('ADD PATH')

n_augmentations = 20

#We have CHONS + Mg + Zn in our proteins (and really nothing else)
unique_atom_types = ['C', 'H', 'MG', 'N', 'O', 'S', 'ZN']

# 26 residue types: the 20AA + HOH + Mg + Zn + the histitine protonation states HIE, HID, HSP
unique_restypes = ['ALA', 'ARG', 'ASN', 'ASP', 'CSS', 'CYH', 'GLN', 'GLU', 'GLY', 'HID', 'HIE', 'HOH', 'HSP',
            'ILE', 'LEU', 'LYS', 'MET', 'MG', 'PHE', 'PRO', 'SER', 'THR', 'TRP', 'TYR', 'VAL', 'ZN']

# the lists fed to the one hot encoder will be sorted, so they should be in alphabetical order always
atom_type_key, atom_type_encoding = featurizer.convert_categorical_to_binary(unique_atom_types ,unique_atom_types)
restype_key, restype_encoding = featurizer.convert_categorical_to_binary(unique_restypes ,unique_restypes)

def imageLoader(files, batch_size):

####use this framework to augment on the fly!!!

    L = len(files)

    #this line is just to make the generator infinite, keras needs that    
    while True:

        batch_start = 0
        batch_end = batch_size

        while batch_start < L:
            limit = min(batch_end, L)

            #now we process:
            training_data = []
            training_labels=[]

          
            for f in fileList[batch_start:limit]:
                with open(f, 'rb') as f:
                    d = pickle.load(f, encoding='latin1')

                center = d.get('center')
                energy = d.get('energy')
                stamp = d.get('stamp')
                atoms = d.get('atoms')

                coord_array = []
                atom_array = []
                for each_atom in atoms:
                    x = each_atom.get('x_coord')
                    y = each_atom.get('y_coord')
                    z = each_atom.get('z_coord')
                    
                    atom_type = each_atom.get('elem')
                    # throw error if we encounter an atom different than ['C', 'H', 'O', 'N', 'S','MG', 'ZN']
                    if atom_type not in unique_atom_types:
                        raise ValueError("Atom type %i is not expected!" % atom_type)
                        
                    atom_restype = each_atom.get('residue_type')
                    if atom_restype not in unique_restypes:
                        raise ValueError("Residue type %i is not expected!" % atom_restype)        
                    
                    coord_array.append([x,y,z])
                    
                    if 'phoenix_charge' not in each_atom:
                        each_atom['phoenix_charge'] = 0

                    this_atom_ffparams=[
                    each_atom['dreiding_BR'],
                    each_atom['dreiding_EA'],
                    each_atom['dreiding_LJ_D0'],
                    each_atom['dreiding_LJ_R0'],
                    each_atom['dreiding_LJ_scale'],
                    each_atom['phoenix_charge'],
                    each_atom['radius'],
                    each_atom['rosetta_lj_d0'],
                    each_atom['rosetta_lj_r0'],
                    each_atom['rosetta_lj_r0_soft'],
                    each_atom['rosetta_lk_dgfree'],
                    each_atom['rosetta_lk_lambda'],
                    each_atom['rosetta_lk_volume'],
                    each_atom['scwrl_radius']
                    ]
                    
                    # create the list of features for this atom
                    this_atom_features = []
                    # entries 0,1,2 are coordinates x,y,z
                    this_atom_features.extend([x,y,z])
                    # entries 3-10 are the 7 atom types one hot encoded, CHONS + Mg + Zn
                    this_atom_features.extend(atom_type_encoding[atom_type_key[atom_type]]) 
                    # entries 11-36 are the 26 residue types
                    this_atom_features.extend(restype_encoding[restype_key[atom_restype]])    
                    # entires 37-50 are the 14 force field parameters to extracted above
                    this_atom_features.extend(this_atom_ffparams)
                    
                    atom_array.append(this_atom_features)

                # checks for bad things, hopefully nothing happens
                bad_indicies =[]
                idx=0
                for types in atom_array:
                    if types == None:
                        bad_indicies.append(idx)
                    idx=idx+1
                for bad in bad_indicies:
                    del atom_array[bad]
                    del coord_array[bad]
                if bad_indicies: # if the bad_indicies list is not empty, show bad atoms
                    print('Found the following bad atoms:', bad_indicies)
                
                # remember entries 0,1,2 are X Y Z cartesian coordinates
                featurized_atoms = np.array(atom_array)
                # note that coord_array==featurized_atoms[:,0:3]
                coord_array = np.array(coord_array)
                center = np.array(center)
                
                augmented_test_coordinates, augmented_centers = featurizer.augment_dataset(coord_array, n_augmentations, center)

                center_index = 0
                for coordinates in augmented_test_coordinates:
                    center = augmented_centers[center_index]
                    # featurized_atoms[:,3:50] passes the 47 features that are not XYZ coordinates
                    voxel_grid = featurizer.voxelize(coordinates, featurized_atoms[:,3:50], center)
                    center_index = center_index + 1

                    #this routine will compress iteratively, but fuck it
                    #comp = io.BytesIO()
                    #np.save(comp, voxel_grid)
                    #compressed = zlib.compress(comp.getvalue())
                    #training_data.append(compressed)
                    
                    training_data.append(voxel_grid)
                    training_labels.append(energy)
                
            #test feeding into offline keras model
            #then need to look at active learning frameworks

            X = training_data
            Y = training_labels


            yield (X,Y) #a tuple with two numpy arrays with batch_size samples     

            batch_start += batch_size   
            batch_end += batch_size
