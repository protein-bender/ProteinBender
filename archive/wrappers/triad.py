from triadPython import *
import numpy
import copy

DEFAULT_AMINO_ACIDS = [
    "ALA", "ARG", "ASN", "ASP", "GLU", "GLN", "GLY", "HIS", "ILE", "LEU", \
    "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"
]


def get_energy_of_repack(
        site_to_design, pdb_file_path,
        amino_acids=DEFAULT_AMINO_ACIDS,
        randomize=True,
        num_trajectories=1000):



    options = GlobalOptionSet(ffset="crosetta")
    options.setAllRosettaOptions()

    # if randomize:
    #     options.job.randomSeed = numpy.random.randint(100000)
    #
    # options.allowRepulsiveReweighting()
    # options.optimization.numTrajectories = num_trajectories
    # options.rotamer.pedanticAttributes = True
    # options.optimization.energyModel = EnergyModelType.Standard
    # options.optimization.startFrom = StartFrom.Current
    # options.optimization.saveLevel = SaveLevel.Accepted
    # options.rotamer.singlesEnergyMax = None
    # options.rotamer.allowCurrent = True
    #
    # # Triad didn"t do this in relaxOptions but we think we want to
    # options.optimization.mover.preserveFormType = True
    #
    # # Turn all terms on
    # options.optimization.setupAllInteractions = True

    # Make the sitemap
    options.optimization.siteMap = makeSiteMap([site_to_design], amino_acids)

    # Create optimizer object
    optimizer = MolecularOptimizer()

    fff, ms = finalize(options, pdb_file_path)

    print("Initial monomer is %s" % ms.getMonomer(site_to_design).getResType())

    # ############ RELAX ITLL BE OKAY ########################
    #
    # options.optimization.selectMover(multiRamp)
    #
    # # Relax the structure, covalent terms are on. This yields our relaxed PDB file.
    # optimization = optimizer.optimize(options, fff=fff, ms=ms)
    # ms = optimization.getMolecularHierarchy()
    #
    # print("After relaxing energy is %.4f" % optimization.getEnergy())
    # print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())
    #

    ############ DISABLE COVALENT ###############

    options.optimization.startFrom = StartFrom.Current
    options.optimization.mover.preserveFormType = True
    options.optimization.selectMover(disableCovalent)

    optimization = optimizer.optimize(options, fff=fff, ms=ms)
    ms = optimization.getMolecularHierarchy()

    print("After disable covalent energy is %.4f" % optimization.getEnergy())
    print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())

    ############ PACK THAT SHIT ########################

    options.optimization.startFrom = StartFrom.Random
    options.optimization.mover.preserveFormType = False
    options.rotamer.allowCurrent = False

    options.optimization.selectMover(rosettaRepackingMover)

    # Now perform the top rotamer selection
    optimization = optimizer.optimize(options, fff=fff, ms=ms)
    ms = optimization.getMolecularHierarchy()

    print("After repacking energy is %.4f" % optimization.getEnergy())
    print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())

    ############ DISABLE COVALENT ###############

    options.optimization.startFrom = StartFrom.Current
    options.optimization.mover.preserveFormType = True
    options.optimization.selectMover(disableCovalent)

    optimization = optimizer.optimize(options, fff=fff, ms=ms)
    ms = optimization.getMolecularHierarchy()

    optimization.energyAnalysis()

    repacked_molecular_system = copy.deepcopy(ms)

    before = optimization.getEnergy()

    print("After repacking and disable covalent energy is %.4f" % optimization.getEnergy())
    print("Monomer is now %s" % ms.getMonomer(site_to_design).getResType())

    ############ ENABLE COVALENT ###############

    options.optimization.selectMover(enableCovalent)

    optimization = optimizer.optimize(options, fff=fff, ms=ms)
    ms = optimization.getMolecularHierarchy()

    ############ RELAX IT"LL BE OKAY ########################

    options.optimization.startFrom = StartFrom.Current
    options.optimization.mover.preserveFormType = True

    options.optimization.selectMover(multiRamp)

    # Relax the structure, covalent terms are on. This yields our relaxed PDB file.
    optimization = optimizer.optimize(options, fff=fff, ms=ms)

    after = optimization.getEnergy()

    return before, after, repacked_molecular_system
