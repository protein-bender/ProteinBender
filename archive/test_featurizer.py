from ml import featurizer
import numpy
import time
import sys
import socket
import pickle
import struct

HOST = "127.0.0.1"
PORT = 38338
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conn, address = s.accept()
print("Connected by ", address)
while True:

    length_data = b''
    while len(length_data) < 4:
        packet = conn.recv(4 - len(length_data))
        if not packet:
            raise ValueError("Bad value in socket ahhh")
        length_data += packet

    message_length = struct.unpack(">I", length_data)[0]

    print("Length of message: %i" % message_length)

    data = b''
    while len(data) < message_length:
        packet = conn.recv(message_length - len(data))
        if not packet:
            raise ValueError("Bad value in socket ahhh")
        data += packet

    received = pickle.loads(data)

    print(received)

    num_dimensions = 3
    num_atoms = 100

    atom_types = ["H", "C", "N", "O"]

    numpy.random.seed(42)
    test_coordinates = numpy.random.normal(0, 1, (num_atoms, num_dimensions)).round(4)
    atoms = numpy.random.choice(atom_types, num_atoms)

    _, atoms = featurizer.convert_categorical_to_binary(atoms, atom_types)

    start_time = time.time()

    augmented_test_coordinates = featurizer.augment_dataset(test_coordinates, 1000)

    for coordinates in augmented_test_coordinates:
        voxel_grid = featurizer.voxelize(coordinates, atoms)

    duration = time.time() - start_time

    print("Duration: %.2f" % duration)


    time.sleep(1)


conn.close()