# Bend all the proteins

from ml import featurizer
from ml.model import Model
from wrappers import triad as triad_wrapper


# Some globals
MAX_NUM_RELAXATIONS = 5


# Input data
#  - PDB
#  - List of sites
#  - Acceptable amino acids

model = Model()

# Site picker
#  - Choose a site
num_relaxations = 0
delta_energy = 0

while num_relaxations < MAX_NUM_RELAXATIONS and delta_energy < 0:

    # Triad starter
    #  - Return top eligible rotamer
    #  - Crop around center of current AA

    # Voxelizer
    #  - Get XYZ + atomic properties of crop cube
    #  - voxelizes

    # Predictor
    #  - Tell me if I should relax
    relax = model.predict(voxel_grid)


    if relax:
        triad_wrapper.relax()
        num_relaxations += 1
    else:
