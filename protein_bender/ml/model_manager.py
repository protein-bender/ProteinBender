import socket
import struct
import pickle
import numpy
from threading import Thread
from functools import partial
import time
import datetime
import os

from ml import featurizer
from utils import Request_Type
from utils import utils as pb_utils


HOST = "127.0.0.1"


class Model_Manager:

    def __init__(self, model, training_directory=None,
                 working_directory=os.getcwd()):

        self._model = model
        self._training_directory = training_directory
        self._client_threads = []
        self._is_listening = False
        self._working_directory = working_directory
        self._log_file = None
        self._init_time = datetime.datetime.utcnow()

        self.initialize_log_file()

    def log_message(self, message):

        time_now = datetime.datetime.utcnow()
        time_now_string = time_now.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

        log_string = "[%s]: %s" % (time_now_string, message)

        print(log_string)
        self._log_file.write(log_string)

    def initialize_log_file(self):

        if not os.path.exists(self._working_directory):
            os.makedirs(self._working_directory)

        file_time_string = self._init_time.strftime("%Y%m%dT%H%M%SZ")
        log_file_name = "model_manager_" + file_time_string + ".log"

        self._log_file = open(os.path.join(self._working_directory,
                                           log_file_name), "w")

        self.log_message("Log file initialized")

    def listen_for_requests(self, client_stream, address):

        self.log_message("Connected by %s:%i" % address)

        while self._is_listening:

            try:
                length_bytes = b''
                while len(length_bytes) < 4:
                    packet = client_stream.recv(4 - len(length_bytes))
                    if not packet:
                        raise ValueError("Bad value in socket ahhh")
                    length_bytes += packet

                data_length = struct.unpack(">I", length_bytes)[0]

                self.log_message("Length of data: %i" % data_length)

                request_type_bytes = b''
                while len(request_type_bytes) < 4:
                    packet = client_stream.recv(4 - len(request_type_bytes))
                    if not packet:
                        raise ValueError("Bad value in socket ahhh")
                    request_type_bytes += packet

                request_type = struct.unpack(">I", request_type_bytes)[0]

                if request_type == Request_Type.PREDICT:

                    self.log_message("Received prediction request")

                    feature_bytes = b''
                    while len(feature_bytes) < data_length:
                        packet = client_stream.recv(data_length -
                                                    len(feature_bytes))
                        if not packet:
                            raise ValueError("Bad value in socket ahhh")
                        feature_bytes += packet

                    features = pickle.loads(feature_bytes, encoding="latin1")

                    if type(features) is not numpy.ndarray:

                        coord_array, features, center = \
                            pb_utils.convert_atom_dict_to_coordinate_feature_list(
                                features)

                        # We need to make sure we use the same voxel size/scale
                        # here as the original data. Probably should be part of
                        # the config
                        voxel_grid = featurizer.voxelize(coord_array, features,
                                                         center=center)
                    else:
                        voxel_grid = features

                    prediction = self._model.predict(voxel_grid)

                    self.log_message("Sending prediction of %.4f" % prediction)
                    prediction_bytes = struct.pack(">d", prediction)

                    client_stream.sendall(prediction_bytes)

                elif request_type == Request_Type.INFORM:

                    self.log_message("Received inform request")

                    label_bytes = b''
                    while len(label_bytes) < 8:
                        packet = client_stream.recv(8 - len(label_bytes))
                        if not packet:
                            raise ValueError("Bad value in socket ahhh")
                        label_bytes += packet

                    label = struct.unpack(">d", label_bytes)

                    self.log_message("Training example label is %.2f" % label)

                    feature_bytes = b''
                    while len(feature_bytes) < data_length:
                        packet = client_stream.recv(data_length -
                                                    len(feature_bytes))
                        if not packet:
                            raise ValueError("Bad value in socket ahhh")
                        feature_bytes += packet

                    features = pickle.loads(feature_bytes, encoding="latin1")

                    if type(features) is not numpy.ndarray:

                        coord_array, features, center = pb_utils.\
                            convert_atom_dict_to_coordinate_feature_list(
                                features)

                        # We need to make sure we use the same voxel size/scale
                        # here as the original data. Probably should be part of
                        # the config
                        voxel_grid = featurizer.voxelize(coord_array, features,
                                                         center=center)
                    else:
                        voxel_grid = features

                    self._model.add_training_example(voxel_grid, label)
                else:
                    print("Got invalid request type from client!")

            except socket.timeout:
                continue
            except ValueError:
                break

        client_stream.close()

    def listen_for_clients(self, port, host=HOST):

        print("Listening on port %i" % port)

        self._is_listening = True

        listener_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        listener_socket.bind((host, port))
        listener_socket.listen()
        listener_socket.settimeout(5)

        while self._is_listening:

            try:
                client_stream, address = listener_socket.accept()
            except socket.timeout:
                continue

            if client_stream is None:
                continue

            client_thread = Thread(
                target=partial(self.listen_for_requests,
                               client_stream, address))

            client_thread.start()
            self._client_threads.append(client_thread)

    def stop(self, signal, frame):
        self._is_listening = False
