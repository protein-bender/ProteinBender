import numpy
import socket
import pickle
import struct

from utils import Request_Type


class Predictor(object):

    def __init__(self):
        pass

    def predict(self, atom_dict):
        pass

    def inform(self, atom_dict, label):
        pass


class Network_Predictor(object):

    def __init__(self, port):
        super(Network_Predictor, self).__init__()
        self._port = port

        self._hostname = "127.0.0.1"

        try:
            self._predictor_socket = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
            self._predictor_socket.connect((self._hostname, port))
        except socket.error:
            raise EnvironmentError(
                "Unable to connect to server %s on port %i. "
                "Is the predictor running?" % (self._hostname, port))

    def predict(self, atom_dict):

        payload = pickle.dumps(atom_dict, 2)

        data = struct.pack(">I", len(payload)) + \
            struct.pack(">I", Request_Type.PREDICT) + payload

        self._predictor_socket.sendall(data)

        prediction_bytes = b''
        while len(prediction_bytes) < 8:
            packet = self._predictor_socket.recv(
                8 - len(prediction_bytes))
            if not packet:
                raise ValueError("Bad value in socket ahhh")
            prediction_bytes += packet

        prediction = struct.unpack(">d", prediction_bytes)

        return prediction

    def inform(self, atom_dict, label):

        payload = pickle.dumps(atom_dict, 2)

        data = struct.pack(">I", len(payload)) + \
            struct.pack(">I", Request_Type.INFORM) + \
            struct.pack(">d", label) + payload

        self._predictor_socket.sendall(data)


class Random_Predictor(object):

    def __init__(self, threshold=0.5):
        super(Random_Predictor, self).__init__()
        self._threshold = threshold

    def predict(self, atom_dict):
        return numpy.random.random() > self._threshold, 1.0

    def inform(self, atom_dict, label):
        pass
