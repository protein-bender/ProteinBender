import os
import json
import codecs
from triadPython import GlobalOptionSet, makeSiteMap, disableCovalent, \
    finalize, MolecularOptimizer, rosettaRepackingMover, multiRamp
import time
import datetime
from utils import triad as triad_wrapper

DEFAULT_AMINO_ACIDS = [
    "ALA", "ARG", "ASN", "ASP", "GLU", "GLN", "GLY", "HIS", "ILE", "LEU", "LYS",
    "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"
]

MAX_ROTAMER_ATTEMPTS = 10


class Protein_Bender(object):

    def __init__(self, protein_file_path):

        self._protein_file_path = protein_file_path
        self._working_directory = os.getcwd()
        self._predictor = None
        self._amino_acids = DEFAULT_AMINO_ACIDS
        self._sites_to_design = None
        self._confidence_threshold = 0.9
        self._init_time = None
        self._log_file = None
        self._max_rotamer_attempts = MAX_ROTAMER_ATTEMPTS

    def load_config_file(self, config_file_path):

        config_file = codecs.open(config_file_path, "r")
        config = json.loads(config_file.read())

        if "protein_file_path" in config:
            self._protein_file_path = str(config["protein_file_path"])

        if "working_directory" in config:
            self._working_directory = config["working_directory"]

        if "amino_acids" in config:
            self._amino_acids = [amino_acid.encode("ascii") for
                                 amino_acid in config["amino_acids"]]

        if "sites_to_design" in config:
            self._sites_to_design = [site.encode("ascii") for
                                     site in config["sites_to_design"]]

        config_file.close()

    def log_message(self, message):

        time_now = datetime.datetime.utcnow()
        time_now_string = time_now.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

        log_string = "[%s]: %s" % (time_now_string, message)

        print(log_string)
        self._log_file.write(log_string)

    def initialize_log_file(self):

        if not os.path.exists(self._working_directory):
            os.makedirs(self._working_directory)

        file_time_string = self._init_time.strftime("%Y%m%dT%H%M%SZ")
        log_file_name = "protein_bender_" + file_time_string + ".log"

        self._log_file = open(os.path.join(self._working_directory,
                                           log_file_name), "w")

        self.log_message("Log file initialized")

    def start_bending(self):

        self._init_time = datetime.datetime.utcnow()

        self.initialize_log_file()

        if self._sites_to_design is None:
            raise ValueError("Must specify sites to design before bending!")

        if self._predictor is None:
            raise ValueError("Must specify a predictor before bending!")

        # Now we are going to design one site at a time
        for site_to_design in self._sites_to_design:

            self.log_message("Designing site %s" % site_to_design)

            found_good_rotamer = False

            attempt_number = 1

            while not found_good_rotamer and \
                    attempt_number <= self._max_rotamer_attempts:

                self.log_message("Starting rotamer attempt %i" % attempt_number)

                options = GlobalOptionSet(ffset="crosetta")
                options.setAllRosettaOptions()
                options.optimization.setupAllInteractions = True

                options.optimization.siteMap = makeSiteMap(
                    [site_to_design], self._amino_acids)

                options.optimization.selectMover(disableCovalent)

                # Do the initial loop optimization based on the wild-type sequence
                fff, ms = finalize(options, protein_file_path)
                optimizer = MolecularOptimizer()
                optimization = optimizer.optimize(options, fff, ms)

                optimization.getAnalysis()
                options.optimization.selectMover(rosettaRepackingMover)
                optimization = optimizer.optimize(options, fff, ms)
                optimization.getAnalysis()
                before = optimization.energyAnalysis(0)
                print("Before: %.4f" % before)

                atom_dict = triad_wrapper.convert_molecular_system_to_dict(
                    ms, site_to_design)

                # If the predictor predicts this is not a good relaxation, we
                # keep going
                prediction, confidence = self._predictor.predict(atom_dict)

                if confidence < self._confidence_threshold:
                    # this block is for extraction to voxelizer

                    optimizer.applyMover(options, disableCovalent)
                    options.optimization.selectMover(multiRamp)
                    optimization = optimizer.optimize(
                        options, fff=None, ms=None, poseGenerators=[]
                    )
                    optimization.getAnalysis()
                    after = optimization.energyAnalysis(0)
                    print("After: %.4f" % before)

                    energy_change = before - after
                    millis = int(round(time.time() * 1000))

                    #TODO: Inform the model here :v
                elif prediction is True:
                    found_good_rotamer = True
                else:
                    found_good_rotamer = False

                attempt_number += 1

    def save(self):
        pass

    @property
    def predictor(self):
        return self._predictor

    @predictor.setter
    def predictor(self, new_predictor):
        self._predictor = new_predictor

    @property
    def working_directory(self):
        return self._working_directory

    @working_directory.setter
    def working_directory(self, new_working_directory):
        self._working_directory = new_working_directory
