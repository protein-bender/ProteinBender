import sys
import os
import time

from triadPython import *
import numpy
import copy

DEFAULT_AMINO_ACIDS = [
    "ALA", "ARG", "ASN", "ASP", "GLU", "GLN", "GLY", "HIS", "ILE", "LEU", \
    "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"
]


def full_relax_without_design(pdb_file_path, num_trajectories=1000,
                              pdb_out_path=None,
                              randomize=True):
    """
    Relaxes a protein without performing any amino acid substitutions. Relaxes
    the full protein.

    :param pdb_file_path: The path to a PDB file to relax
    :param num_trajectories: How many sequential steps to search. Dramatically
                             increases the runtime. (5 trajectories ~ a few
                             minutes)
    :param pdb_out_path: The path to write out the resulting PDB file, if any
    :param randomize: Whether to randomize each start (default: True)
    :return: (energy, molecular_system) The resulting energy and the molecular
             system containing the structure
    """

    options = GlobalOptionSet(ffset='crosetta')
    options.setAllRosettaOptions()
    options.relaxOptions()
    options.optimization.setupAllInteractions = True

    if randomize:
        options.job.randomSeed = numpy.random.randint(100000)


    print("Num trajectories: %i" % num_trajectories)

    options.optimization.numTrajectories = num_trajectories

    if pdb_out_path is not None:
        options.analysis.outputName = pdb_out_path

    # Make the sitemap
    options.optimization.addSiteMapRule("add", ["WT"], "/")
    options.optimization.selectMover(multiRamp)

    optimizer = MolecularOptimizer()

    fff, ms = finalize(options, pdb_file_path)

    optimization = optimizer.optimize(options, fff=fff, ms=ms)
    ms = optimization.getMolecularHierarchy()

    energy = optimization.getEnergy()

    if pdb_out_path is not None:
        writePDB(options, ms, pdb_out_path)

    return energy, ms


def single_site_design(site_to_design,
                       pdb_file_path,
                       amino_acids=DEFAULT_AMINO_ACIDS,
                       randomize=True,
                       pdb_out_path=None):
    """
    Performs a search to find the best alternative amino acid/rotamer at the
     given site of a protein. Based on a rough estimate of the energy.

    :param site_to_design: Which site to design, in pid format (e.g. A_13)
    :param pdb_file_path: The path to a PDB file to relax
    :param amino_acids: Which amino acids to allow at the site
    :param randomize: Whether to randomize each start (default: True)
    :param pdb_out_path: The path to write out the resulting PDB file, if any
    :return: (energy, molecular_system) The resulting energy and the molecular
             system containing the structure
    """

    options = GlobalOptionSet(ffset="rosetta")
    options.setAllRosettaOptions()
    options.optimization.setupAllInteractions = True
    options.optimization.siteMap = makeSiteMap([site_to_design], amino_acids)

    if randomize:
        options.job.randomSeed = numpy.random.randint(100000)

    if pdb_out_path is not None:
        options.analysis.outputName = pdb_out_path

    optimizer = MolecularOptimizer()

    fff, ms = finalize(options, pdb_file_path)

    original_residue = ms.getMonomer(site_to_design).getResType()

    options.optimization.selectMover(rosettaRepackingMover)

    # Now perform the top rotamer selection
    optimization = optimizer.optimize(options, fff=fff, ms=ms)
    ms = optimization.getMolecularHierarchy()

    energy = optimization.getEnergy()

    new_residue = ms.getMonomer(site_to_design).getResType()

    if original_residue == new_residue:
        print("Repacking mover kept residue '%s', aborting" % original_residue)
        return None, None

    print("Repacking mover swapped residue '%s' to '%s'" % (original_residue, new_residue))

    if pdb_out_path is not None:
        writePDB(options, ms, pdb_out_path)

    return energy, ms


def get_energy_of_repack(site_to_design, pdb_file_path,
                         amino_acids=DEFAULT_AMINO_ACIDS,
                         randomize=True,
                         num_trajectories=1000):
    """
    Performs a sequence of Triad operations to explore the effect of a single
    amino acid substitution on the relaxed energy of a protein. Returns the
    pre-relaxed molecular system and the different energies

    :param site_to_design: Which site to design, in pid format (e.g. A_13)
    :param pdb_file_path: The path to a PDB file to relax
    :param amino_acids: Which amino acids to allow at the site
    :param randomize: Whether to randomize each start (default: True)
    :param num_trajectories: How many sequential steps to search. Dramatically
                             increases the runtime. (5 trajectories ~ a few
                             minutes)
    :return: (before, after, repacked_molecular_system) The energy before and
             after relaxation and the molecular system containing the structure
             before relaxation
    """


    options = GlobalOptionSet(ffset="crosetta")
    options.setAllRosettaOptions()

    if randomize:
        options.job.randomSeed = numpy.random.randint(100000)

    options.allowRepulsiveReweighting()
    options.optimization.numTrajectories = num_trajectories
    options.rotamer.pedanticAttributes = True
    options.optimization.energyModel = EnergyModelType.Standard
    options.optimization.startFrom = StartFrom.Current
    options.optimization.saveLevel = SaveLevel.Accepted
    options.rotamer.singlesEnergyMax = None
    options.rotamer.allowCurrent = True

    # Triad didn"t do this in relaxOptions but we think we want to
    options.optimization.mover.preserveFormType = True

    # Turn all terms on
    options.optimization.setupAllInteractions = True

    options.optimization.siteMap = makeSiteMap([site_to_design], amino_acids)

    # Create optimizer object
    optimizer = MolecularOptimizer()

    fff, ms = finalize(options, pdb_file_path)

    ############ RELAX ITLL BE OKAY ########################

    options.optimization.selectMover(multiRamp)

    # Relax the structure, covalent terms are on. This yields our relaxed PDB file.
    optimization = optimizer.optimize(options, fff=fff, ms=ms)
    ms = optimization.getMolecularHierarchy()

    before = optimization.getEnergy()

    ############ PACK THAT SHIT ########################

    options.optimization.startFrom = StartFrom.Random
    options.optimization.mover.preserveFormType = False

    options.optimization.selectMover(rosettaRepackingMover)

    # Now perform the top rotamer selection
    optimization = optimizer.optimize(options, fff=fff, ms=ms)
    ms = optimization.getMolecularHierarchy()

    repacked_molecular_system = copy.deepcopy(ms)
    before = optimization.getEnergy()

    ############ RELAX IT"LL BE OKAY ########################

    options.optimization.startFrom = StartFrom.Current
    options.optimization.mover.preserveFormType = True

    options.optimization.selectMover(multiRamp)

    # Relax the structure, covalent terms are on. This yields our relaxed PDB file.
    optimization = optimizer.optimize(options, fff=fff, ms=ms)

    after = optimization.getEnergy()

    return before, after, repacked_molecular_system


def get_energy_of_repack_pdb(site_to_design, pdb_file_path,
                             amino_acids=DEFAULT_AMINO_ACIDS,
                             randomize=True,
                             num_trajectories=1000):
    """
    Performs a sequence of Triad operations to explore the effect of a single
    amino acid substitution on the relaxed energy of a protein. Returns the
    pre-relaxed molecular system and the different energies

    Similar functionality to get_energy_of_repack, except that it saves
    each processing step to an intermediate PDB file, increasing processing
    time but decrease

    :param site_to_design: Which site to design, in pid format (e.g. A_13)
    :param pdb_file_path: The path to a PDB file to relax
    :param amino_acids: Which amino acids to allow at the site
    :param randomize: Whether to randomize each start (default: True)
    :param num_trajectories: How many sequential steps to search. Dramatically
                             increases the runtime. (5 trajectories ~ a few
                             minutes)
    :return: (WT, before, after, repacked_molecular_system) The energy of the WT
             sequence, before and after relaxation and the molecular system
             containing the structure before relaxation
    """

    pdb_rand_seed = numpy.random.randint(1000000) #prevent accidental overwrites
    tmp_pdb_file_path = pdb_file_path + str(pdb_rand_seed) + site_to_design + ".tmp"

    start = time.time()

    energy_WT_relaxed, molecular_system_WT_relaxed = full_relax_without_design(
        pdb_file_path=pdb_file_path,
        pdb_out_path=tmp_pdb_file_path,
        randomize=randomize,
        num_trajectories=num_trajectories
    )

    end = time.time()

    print("Relax duration: %.4f" % (end - start))

    print("WT relax energy: %.4f" % energy_WT_relaxed)

    energy_designed, molecular_system_designed = single_site_design(
        site_to_design=site_to_design,
        pdb_file_path=tmp_pdb_file_path,
        pdb_out_path=tmp_pdb_file_path,
        amino_acids=amino_acids,
        randomize=randomize
    )

    if energy_designed is None:
        os.unlink(tmp_pdb_file_path)
        return None, None, None, None

    print("Designed energy: %.4f" % energy_designed)

    energy_designed_relaxed, molecular_system_designed_relaxed = \
        full_relax_without_design(
            pdb_file_path=tmp_pdb_file_path,
            pdb_out_path=None,
            randomize=randomize,
            num_trajectories=num_trajectories
        )

    print("Designed relaxed energy: %.4f" % energy_designed_relaxed)

    os.unlink(tmp_pdb_file_path)

    return energy_WT_relaxed, energy_designed, energy_designed_relaxed, \
           molecular_system_designed
